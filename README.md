# Anaton
A game to find longest anagrams
by [Codefish](mailto:codefish@online.fr)

This JavaScript web application relies on the Svelte framework.

- First, do not forget to download the required dictionaries: `npm run downloadDictionaries`
- To test the project: `npm run dev`
- To build the project: `npm run build`
