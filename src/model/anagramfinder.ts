import type { Dictionary } from "./dictionary";

export class AnagramFinder {
    constructor(private dictionary: Dictionary) {}

    public findAnagrams(draw: string, minLetters: number) {
        let result: string[] = []
        let candidates = [draw]
        while (candidates.length > 0 && (result.length == 0 || result[0].length === candidates[0].length)) {
            let [candidate, ...newCandidates] = candidates
            candidates = newCandidates
            let theClass = this.dictionary.findClass(candidate)
            if (theClass.length > 0)
                result = [...result, ...theClass]
            else if (candidate.length - 1 >= minLetters) {
                let letterSet = new Set(candidate.split(''))
                letterSet.forEach(letterToRemove => {
                    let withoutLetter = candidate.replace(letterToRemove, '')
                    candidates.push(withoutLetter)
                })
            }
        }
        return Array.from(new Set(result).values())
    }
}