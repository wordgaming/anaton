
export function sortLetters(word: string) {
    let a = word.split('')
    a.sort()
    return a.join('')
}

export function removeDiacritics(str: string) {
    return str.normalize("NFKD").replace(/[\u0300-\u036f]/g, "")
}


export class Dictionary {
    private cumulatedFrequencies: [string, number][]
    
    constructor(public readonly name: string, private readonly content: Map<string, string>, letterFrequencies: Map<string, number>, public readonly size: number) {
        this.cumulatedFrequencies = (() => {
            let result = []
            let previous = 0
            for (const [k, v] of letterFrequencies.entries()) {
                result.push([k, previous + v])
                previous = previous + v
            }
            return result
        })()
    }

    private static lastLoadedDictionary: Dictionary|null = null

    public static async load(name: string) {
        if (this.lastLoadedDictionary !== null && this.lastLoadedDictionary.name === name)
            return this.lastLoadedDictionary
        const response = await fetch(`dictionaries/${name}.txt`)
        const textLines = await response.text()
        const result = new Map<string, string>()
        const frequencies = new Map<string, number>()
        let size = 0
        for (let pos = 0; pos < textLines.length; ) {
            let endPos = textLines.indexOf('\n', pos)
            if (endPos < 0) endPos = textLines.length
            let word = textLines.substring(pos, endPos).trim()
            let normalizedWord = removeDiacritics(word.toLowerCase())
            let sortedWord = sortLetters(normalizedWord)
            if (normalizedWord.length > 0 && normalizedWord.search(/[^a-z]/i) < 0) {
                let collection = result.get(sortedWord)
                collection = collection ? `${collection};${normalizedWord}` : normalizedWord
                result.set(sortedWord, collection)
                // update frequencies for word
                for (let letter of sortedWord.split(''))
                    frequencies.set(letter, (frequencies.get(letter) | 0) + 1)
                size += 1
            }
            pos = endPos + 1
        }
        const dict = new Dictionary(name, result, frequencies, size)
        this.lastLoadedDictionary = dict
        return dict
    }

    public findClass(word: string): string[] {
        let res = this.content.get(sortLetters(word))
        return res ? res.split(';') : []
    }

    public isWordPresent(word: string): boolean {
        return this.findClass(word).indexOf(word) >= 0
    }

    public drawLetter() {
        let i = Math.floor(Math.random() * this.cumulatedFrequencies[this.cumulatedFrequencies.length-1][1])
        let j = 0
        for (j = 0; this.cumulatedFrequencies[j][1] < i; j++) ;
        return this.cumulatedFrequencies[j][0]
    }

    public drawLetters(n: number) {
        return Array(n).fill(0).map(() => this.drawLetter()).join('')
    }
}